package pages;

import org.openqa.selenium.WebDriver;
import support.CommonFunctions;
import support.TestProperties;

public class CommonPage {
	private final WebDriver driver;

	public CommonPage(WebDriver driver) {
		this.driver = driver;
	}

	public void launchUrl(String capturedUrl) {
		String constructedUrl = "https://" + TestProperties.loadProperty().getProperty("application.domainName")
				+ capturedUrl;
		driver.navigate().to(constructedUrl);
		CommonFunctions.waitForPageLoad(driver);
	}

}

package support;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonFunctions {
	static final Logger logger = Logger.getLogger(CommonFunctions.class);


	public static void waitForElementClickable(WebDriver driver, WebElement element) {
		try {
			long timeoutSec = Long
					.parseLong(TestProperties.loadProperty().get("browser.ExplicitDrvierWait").toString());
			(new WebDriverWait(driver, timeoutSec)).until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}


	public static void waitForPageLoad(WebDriver driver) {
		try {
			ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return (((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
							.equals("complete")
							&& (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0"));
//					return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
				}
			};

			WebDriverWait wait = new WebDriverWait(driver,
					Integer.parseInt(TestProperties.loadProperty().getProperty("browser.ExplicitDrvierWait")));
			wait.until(expectation);
			Thread.sleep(5000);
		} catch (Exception e) {

		}
	}

	public static void clickLinkElement(WebDriver driver, String linkText) {
		waitForPageLoad(driver);
		WebElement element;
		if (driver.findElements(By.linkText(linkText)).size() > 0) {
			element = driver.findElement(By.linkText(linkText));
		} else if(driver.findElements(By.xpath("//*[contains(text(),'" + linkText + "')]")).size() > 0){
			element = driver.findElement(By.xpath("//*[contains(text(),'" + linkText + "')]"));
		}else{
			element = driver.findElement(By.cssSelector("[title*='"+linkText+"']"));
		}
		CommonFunctions.waitForElementClickable(driver, element);
		element.click();
	}

	public static void clickWebElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String webElementText = element.getText();
		try {
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			CommonFunctions.waitForElementClickable(driver, element);
			js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
	}

}

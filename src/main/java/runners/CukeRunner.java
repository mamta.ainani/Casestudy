package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, features = "src/test/java/features", plugin = {
		"json: cucumber_output/cucumber.json" }, glue = "stepDefinitions", tags = { "@Otech2" })
public class CukeRunner extends AbstractTestNGCucumberTests {
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import Browsers.Browser;

public class Page {
	public WebDriver driver = Browser.getBrowserInstance();
	public CommonPage commonPage = PageFactory.initElements(driver, CommonPage.class);
	public CaseStudyPage caseStudyPage = PageFactory.initElements(driver, CaseStudyPage.class);

}

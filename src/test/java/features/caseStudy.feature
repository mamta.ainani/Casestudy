@caseStudy
Feature: Login
  As a registered user of Otrium
  In order to access the extended features
  I should be able to login to the system

  Background:
#    Given Otrium is launched

  @caseStudy1 @E2E
    Scenario: Registered User should be able to save item in the wish list
    Given I logged into Otrium
  And I select clothing page with search criteria as
  | Clothing | Brand  | Colour |
  | Trousers | adidas | Blue   |
  | Dresses  | Aaiko  | Beige  |
    And I click on "hearts" option
    Then I select hearts on the top right side of the page
    And I verify my item is saved in the favourites

    @caseStudy2  @E2E
    Scenario Outline: User should be able to purchase the item from favourites
      Given Otrium is launched
      And I click on "Ik ga akkoord" for privacy policy
      And I click on Language button
      Then I choose "<lang>" option
      Then I click on "I Agree" link
      And I click on "All brands" link
      And I click on "Search brands" link
      Then I navigate to my "/product/adidas-essentials-stacked-logo-high-rise-leggings-blue" url
      Then I click on "Bestel" button
      And I click on "Naar winkelmand" link
      Then I click on "DOORGAAN MET BETALEN" button
      And I enter my shipping address details as below
        | VoorNaam | AchterNaam | E-mailadres    | Bevestig je e-mailadres | Land      | Postcode | Huisnummer |
        | John     | Smith      | jhjh@gmail.com | jhjh@gmail.com          | Nederland | 1182JR   |  1         |
      Then I click on checkout button

      Examples:
        | lang   | size |
        | Engels |  S   |


      @caseStudy3 @Integration
    Scenario: Verify user should be able to purchase item from the shopping cart
        Given Shopping cart page is launched
        When I verify the "<order list>"
          | Item                          | Brand   | Size   | Quantity | Price |
          | PALMA SNAKE FISH 616 NOISETTE | Aaiko   |  M     |   1      | 40.99 |
        Then I click on "CONTINUE TO CHECKOUT" link
        And I enter the address details
        And I click on "CONTINUE TO PAY" link
        Then I select payment method
        And I click on "CHECKOUT ORDER"
        And I complete the payment
        Then I verify my order purchase

      @caseStudy4 @api
      Scenario Outline: Verify success response by passing all the query parameters
        Given I read the message body from the file "OtriumSearchRequest"
        And I update the request message body with below details
#        This message body is just an example as the api is unknown here
          | includefields | query   | itemcount   | quantity   |
          | <Filter>      | <query> | <itemcount> | <quantity> |
        Given I call "OtriumPurchaseAPI" with "POST" http request and parameter ""
        And I should get response code as "200"

        Examples:
          | Filter  | query                | itemcount | quantity |
          | title   | TITLE-ABS-KEY(java)  | 20        | 2        |

        @caseStudy5 @unit
          Scenario: Verify change country link on the Otrium help desk
          When I navigate to my "Help Desk" page
          And I click on "Change country" button
          Then I choose "English(US)" option
          And I verify "<Expected page title>" is displayed
## Prerequisites

 - Cucumber 4.0 and above ([Click here for documentation](https://cucumber.io/docs/guides/10-minute-tutorial/))
 - Java 11.0 and above
 - Eclipse J2EE version
 

 1. cucumber.tag:
	* To execute individual test scenario :
		- To execute individual test scenario, give the specific tag that is given to each test scenario in feature files for <cucumber.tag> property in pom.xml file.
	* To execute multiple test scenarios in parallel -
		- To execute multiple test scenarios in parallel, give the common tag for test cases i.e. the tag given at feature level for <cucumber.tag> property in pom.xml file. (Example: @documentSearch).
		 - To give multiple tags, (i.e.) if you want to run test cases of documentSearch and advancedSearch then add 
 

### Execution

The framework uses [Maven](https://maven.apache.org/guides/) to execute the tests.

 1. Go to git bash/Eclipse terminal with project location and run the below command 
      " mvn install"
This will download the driver files and places the folder them in your project repository in the below path
	"src/test/resources/selenium_standalone_binaries"
> NOTE: This is one-time step which is needed if you want to run test scenarios in the local machine.
 2. Test scenarios can be executed from git bash
	* From git bash - Go to git bash with project location and run below commands, 
		- mvn clean
		- mvn verify -Pparallel
	

## Consolidated Guidelines

**Checklist of all consolidated guidelines**

 **Naming conventions**
 
- [x] Feature file name should in camel case
		
- [x]  Add step definitions in respective java files that are created based on functionality
		These files should be suffixed with "Steps" and start with a capital letter.
		
- [x] Implement methods for step definitions in respective page object model files that are created based on functionality. These files should be suffixed with "Page" and start with a capital letter

- [x] Methods should be named in camel case
	> Example: Step definition for a step "Given I navigate to Otrium Search form" definition step definition method should be named as "iNavigateToOtriumSearchForm()" as shown below  
		```
		@When("^I navigate to Otrium Search form$")
		public void iNavigateToOtriumSearchForm() {
		}
		```		
		
**Automated and Manual test scenarios**

- [x] All automated regression test scenarios should be placed in one file which will not be having any suffix
		
- [x] All manual regression test scenarios should be placed in another file which will be having a suffix "Manual"
		
- [x] Do not document one-time test scenarios in feature file.
	> Example: UI test cases to check the color of elements, test cases to check a text on a file etc.
		 
**Miscellaneous**
- [x] Do not give credentials in the java files. Add all usernames and passwords in json and call from there. 
- [x] Do not add reporting logs for everything. Only record if something is failed. 
- [x] Avoid defining local variables for the selenium page object locators in the methods. Instead declare them in the class level (beginning of the file) with "@FindBy" annotation.
- [x] Add new test scenarios directly in the enhanced framework
- [x] When you are making changes to the existing features, always run the whole feature in parallel mode before you push it to gitlab.

 

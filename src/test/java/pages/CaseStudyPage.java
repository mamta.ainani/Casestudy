package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import support.CommonFunctions;


public class CaseStudyPage {
    private final WebDriver driver;

    public CaseStudyPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(css = "#__next > div > div.css-y1rj71 > div.css-nq7e2t.eq4tjj20 > div > div > div.css-24rhzu.e1l8whv30 > div.e1l8whv31.css-z3gd2u.e1gxxng20 > button")
    public WebElement langButton;

    @FindBy(css = "#__next > div > div.css-y1rj71 > div.css-nq7e2t.eq4tjj20 > div > div > div.css-24rhzu.e1l8whv30 > div.e1l8whv31.css-z3gd2u.e1gxxng20 > div > div > div > ul > li:nth-child(4) > a")
    public WebElement engOption;

    @FindBy(xpath = "body > div.ot-main > div.page-wrapper > div.page-sales.page-wrapper-grey-old.page-wrap-grey > div.brands-filter-wrap > div.desktop-wrap > div > div.brands.col.col-active > div.menu-wrap.show-menu > div")
    public WebElement dropDownPane;

    @FindBy(how = How.CSS, using = ".menu.transition.visible > div:nth-of-type(1)" )
    public WebElement sizeItem;

    @FindBy(how = How.CSS, using = "#shipping_first_name")
    public WebElement firstName;

    @FindBy(how = How.CSS, using = "#shipping_last_name")
    public WebElement lastName;

    @FindBy(how = How.CSS, using = "#shipping_email")
    public WebElement emailCustomer;

    @FindBy(how = How.CSS, using = "#shipping_email_repeat")
    public WebElement emailRepeatCustomer;

    @FindBy(how = How.CSS, using = "#select2-shipping_country-container")
    public WebElement countryCust;

    @FindBy(how = How.CSS, using = "#shipping_postcode")
    public WebElement postCodeCust;

    @FindBy(how = How.CSS, using = "#shipping_house_number")
    public WebElement houseNumCust;

    @FindBy(css = "#shipping_city")
    public WebElement cityCustomer;

    @FindBy(css = "#shipping_street_name")
    public WebElement streetCust;

 //   @FindBy(css = "div:nth-of-type(1) > .checkout-btn-cnt-new.continue-checkout")
    @FindBy(how =How.CSS, using = "div:nth-of-type(1) > .checkout-btn-cnt-new.continue-checkout")
    public WebElement checkOutBtn;

    public void clickOnLangButton() {
        langButton.click();
    }

    public void clickOnOption(String langName) {
        switch (langName) {
            case "Engels":
                 engOption.click();
                break;
            default:
                Assert.fail(langName + "' is not a option");
                break;
        }
    }

    public void selCheckBox(String checkBoxLabel) {
        WebElement checkBoxLabelWebEle = dropDownPane
                .findElement(By.xpath("//label[contains(text(),'" + checkBoxLabel + "')]"));
        WebElement checkBox = checkBoxLabelWebEle
                .findElement(By.cssSelector(".//div.menu-wrap.show-menu > div > form > label:nth-child(4)::input[@type='checkbox']"));
        if (checkBox.getAttribute("checked") == null) {
            CommonFunctions.clickWebElement(driver, checkBoxLabelWebEle);
        }
    }
    public void clickOnSize(String size) {
        switch (size) {
            case "S":
                sizeItem.click();
                break;
            default:
                Assert.fail(size + "' is not a option");
                break;
        }
    }

    public void enterFirstName(String inputVal) {
       firstName.clear();
       firstName.sendKeys(inputVal); }

    public void enterLastName(String inputVal) {
        lastName.clear();
        lastName.sendKeys(inputVal); }

    public void enterEmailAdd(String inputVal) {
        emailCustomer.clear();
        emailCustomer.sendKeys(inputVal); }

    public void enterEmailConfirm(String inputVal) {
        emailRepeatCustomer.clear();
        emailRepeatCustomer.sendKeys(inputVal); }

    public void enterCountry(String inputVal) {
        countryCust.clear();
        countryCust.sendKeys(inputVal); }

    public void enterPostCode(String inputVal) {
        postCodeCust.clear();
        postCodeCust.sendKeys(inputVal); }

    public void enterHouseNum(String inputVal) {
        houseNumCust.clear();
        houseNumCust.sendKeys(inputVal);
        driver.findElement(By.cssSelector(".checkout-steps")).click();
    }

    public void enterCity(String inputVal) {
        cityCustomer.clear();
        cityCustomer.sendKeys(inputVal); }

    public void enterStreet(String inputVal) {
        streetCust.clear();
        streetCust.sendKeys(inputVal); }

}

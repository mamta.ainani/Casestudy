package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.json.JSONObject;
import pages.Page;
import support.CommonFunctions;

import java.util.List;
import java.util.Map;



public class CaseStudySteps {
    private Page page = new Page();
    public static String address;
    public static JSONObject changeDetails = new JSONObject();

  //  @And("I select wishlist on the top right side of the page")
  @When("^I click on \"([^\"]*)\" for privacy policy$")
          public void iClickOnLink(String linkText) {
      CommonFunctions.clickLinkElement(page.driver, linkText);
  }

  @And("^I click on Language button$")
  public void verifyLang(){
    page.caseStudyPage.clickOnLangButton();
  }
    @Then("^I choose \"([^\"]*)\" option$")
    public void clickOnLang(String langName) {page.caseStudyPage.clickOnOption(langName);}

    @When("^I select required \"([^\"]*)\" checkbox$") public void iSelectCheckbox(String checkBoxTitle) {
        page.caseStudyPage.selCheckBox(checkBoxTitle);

    }

    @When("I navigate to my \"([^\"]*)\" url")
    public void iLaunchUrl(String capturedUrl) {
        page.commonPage.launchUrl(capturedUrl);
    }

    @And("I select size in choose a size dropdown")
    public void iSelectSize()
    {
        page.caseStudyPage.sizeItem.click();
    }
    @Then("^I choose \"([^\"]*)\" option for size$")
    public void clickOnSize(String size) {page.caseStudyPage.clickOnSize(size);}



    @Given("^I enter my shipping address details as below$")
    public void iEnterAddressAsBelow(DataTable inputs) {

        List<Map<String, String>> addressDetails = inputs.asMaps(String.class, String.class);
        for (Map<String, String> addressDetail : addressDetails) {
            page.caseStudyPage.enterFirstName(addressDetail.get("VoorNaam"));
            page.caseStudyPage.enterLastName(addressDetail.get("AchterNaam"));
            page.caseStudyPage.enterEmailAdd(addressDetail.get("E-mailadres"));
            page.caseStudyPage.enterEmailConfirm(addressDetail.get("Bevestig je e-mailadres"));
       //     page.otechPage.enterCountry(addressDetail.get("Land"));
            page.caseStudyPage.enterPostCode(addressDetail.get("Postcode"));
            page.caseStudyPage.enterHouseNum(addressDetail.get("Huisnummer"));
       //     page.otechPage.enterCity(addressDetail.get("Stad"));
       //     page.otechPage.enterStreet(addressDetail.get("Straat"));
            address = "";
            for (String key : addressDetail.keySet()) {
                address = address + ", " + addressDetail.get(key);
            }
            //IPWAdminScreenSteps.changeDetails.put("Secondary address", address.substring(2, address.length()));
        }
    }

    @Then("^I click on checkout button$")
    public void iClickCheckout(){
    //  page.otechPage.checkOutBtn.submit();
      //  CommonFunctions.waitForPageLoad(page.driver);
    //    CommonFunctions.scrollToElement(this.page.driver, page.otechPage.checkOutBtn);
        CommonFunctions.waitForElementClickable(page.driver,page.caseStudyPage.checkOutBtn );
        page.caseStudyPage.checkOutBtn.click();

    }

}

package stepDefinitions;


import Browsers.Browser;
import cucumber.api.java.en.Given;
import pages.Page;
import support.TestProperties;

public class LoginSteps {
    private Page page = new Page();


    @Given("^Otrium is launched$")
    public void launchOtrium() {
        String homePageUrl;
        homePageUrl = "https://" + TestProperties.loadProperty().getProperty("application.domainName");
        try {
            page.driver.navigate().to(homePageUrl);
        } catch (Exception e) {
            Browser.initBrowser();
            page.driver = Browser.getBrowserInstance();
        }


    }




}

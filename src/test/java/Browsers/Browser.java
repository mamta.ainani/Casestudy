package Browsers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import support.TestProperties;

public class Browser {

	private Browser() {

	}

	public static RemoteWebDriver driver;
	public static String binaryURL = TestProperties.loadProperty().getProperty("driver.binaries.path");
	public static String browserBit = TestProperties.loadProperty().getProperty("browserBit");
	public static String operatingSystem = TestProperties.loadProperty().getProperty("operatingSystem");
	public static String browserName = TestProperties.loadProperty().getProperty("browserName");
	public static int globalTimeout = Integer.parseInt(TestProperties.loadProperty().getProperty("browser.ExplicitDrvierWait"));

	public static void initBrowser() {
		startLocalDriver();
		driver.manage().timeouts().implicitlyWait(globalTimeout, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public static WebDriver getBrowserInstance() {
		if (driver == null) {
			initBrowser();
		}
		return driver;
	}


	public static void startLocalDriver() {
		String executable = browserName;
		if (operatingSystem.equalsIgnoreCase("windows")) {
			executable = executable + ".exe";
		}

		switch (browserName.toLowerCase()) {

		case "chromedriver":
			System.setProperty("webdriver.chrome.driver",
					binaryURL + "/" + operatingSystem + "/" + browserBit + "/" + executable);
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("headless");
			driver = new ChromeDriver(options);
			break;
		case "geckodriver":
		case "firefoxdriver":
			System.setProperty("webdriver.gecko.driver",
					binaryURL + "/" + operatingSystem + "/" + browserBit + "/" + executable);
			driver = new FirefoxDriver();
			break;
		}
	}

}

package stepDefinitions;

import Browsers.Browser;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.Page;
import support.CommonFunctions;

public class CommonSteps {
	Page page = new Page();

	@When("^I click on \"([^\"]*)\" link$")
	public void iClickOnLink(String linkText) {
		CommonFunctions.clickLinkElement(page.driver, linkText);
	}

	@When("^I click on \"([^\"]*)\" button")
	public void iClickOnButton(String buttonText) {
		CommonFunctions.clickLinkElement(page.driver, buttonText);
	}




}
